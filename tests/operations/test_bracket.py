import pytest
import numpy as np
from pydantic import ValidationError

from fastmatrix.operations.bracket import BracketProd, bracket_prod


@pytest.mark.parametrize(
    "a,x",
    [
        (np.zeros((3, 3)), np.zeros((3, 3, 3))),
        (np.ones((4, 3)), np.ones((3, 3, 3))),
    ],
)
def test_bracket_prod_correct(a, x):
    BracketProd(matrix_a=a, matrix_x=x)


@pytest.mark.parametrize("a,x", [(np.zeros((1, 1, 1)), np.zeros((1, 1, 1)))])
def test_dims_validation_a(a, x):
    with pytest.raises(KeyError):
        BracketProd(matrix_a=a, matrix_x=x)


@pytest.mark.parametrize("a,x", [(np.zeros((1, 1)), np.zeros((1, 1)))])
def test_dims_validation_x(a, x):
    with pytest.raises(ValidationError):
        BracketProd(matrix_a=a, matrix_x=x)


@pytest.mark.parametrize("a,x", [(np.zeros((3, 2)), np.zeros((3, 3, 3)))])
def test_dims_validation_ax(a, x):
    with pytest.raises(ValidationError):
        BracketProd(matrix_a=a, matrix_x=x)


@pytest.mark.parametrize(
    "a,x,expected",
    [
        (
            np.ones((3, 2)),
            np.array(
                [
                    [[1, 2], [3, 4]],
                    [[5, 6], [7, 8]],
                ]
            ),
            np.array(
                [
                    [[6.0, 8.0], [10.0, 12.0]],
                    [[6.0, 8.0], [10.0, 12.0]],
                    [[6.0, 8.0], [10.0, 12.0]],
                ]
            ),
        )
    ],
)
def test_compute(a, x, expected):
    result_01 = BracketProd(matrix_a=a, matrix_x=x).compute()
    result_02 = bracket_prod(a, x)

    assert np.array_equal(result_01, expected)
    assert np.array_equal(result_02, expected)
