# -*- coding: utf-8 -*-
import pytest
import numpy as np
from pydantic import ValidationError

from fastmatrix.operations.array_multiplication import ArrayMult, array_mult


@pytest.mark.parametrize(
    "a,x,b",
    [
        (np.zeros((2, 3)), np.zeros((3, 3, 3)), np.zeros((3, 2))),
        (np.ones((2, 3)), np.ones((3, 3, 3)), np.ones((3, 2))),
    ],
)
def test_array_mult_correct(a, x, b):
    ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b)


@pytest.mark.parametrize(
    "a,x,b", [(np.zeros((1, 1, 1)), np.zeros((1, 1, 1)), np.zeros((1, 1)))]
)
def test_dims_validation_a(a, x, b):
    with pytest.raises(KeyError):
        ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b)


@pytest.mark.parametrize(
    "a,x,b", [(np.zeros((1, 1)), np.zeros((1, 1)), np.zeros((1, 1)))]
)
def test_dims_validation_x(a, x, b):
    with pytest.raises(KeyError):
        ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b)


@pytest.mark.parametrize(
    "a,x,b", [(np.zeros((1, 1)), np.zeros((1, 1)), np.zeros((1, 1, 1)))]
)
def test_dims_validation_b(a, x, b):
    with pytest.raises(KeyError):
        ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b)


@pytest.mark.parametrize(
    "a,x,b", [(np.zeros((3, 2)), np.zeros((3, 3, 3)), np.zeros((3, 2)))]
)
def test_dims_validation_ax(a, x, b):
    with pytest.raises(KeyError):
        ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b)


@pytest.mark.parametrize(
    "a,x,b", [(np.zeros((3, 3)), np.zeros((3, 3, 3)), np.zeros((2, 2)))]
)
def test_dims_validation_xb(a, x, b):
    with pytest.raises(ValidationError):
        ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b)


@pytest.mark.parametrize(
    "a,x,b,expected",
    [
        (
            np.ones((3, 2)),
            np.array(
                [
                    [[1, 2, 3], [2, 4, 6]],
                    [[2, 4, 6], [4, 8, 12]],
                    [[3, 6, 9], [6, 12, 18]],
                ]
            ),
            np.ones((3, 2)),
            np.array(
                [
                    [[18, 18], [18, 18], [18, 18]],
                    [[36, 36], [36, 36], [36, 36]],
                    [[54, 54], [54, 54], [54, 54]],
                ]
            ),
        )
    ],
)
def test_compute(a, x, b, expected):
    result_01 = ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b).compute()
    result_02 = array_mult(a, x, b)
    assert np.array_equal(result_01, expected)
    assert np.array_equal(result_02, expected)
