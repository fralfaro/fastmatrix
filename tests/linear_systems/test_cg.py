# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
import pytest
import numpy as np
from pydantic import ValidationError

from fastmatrix.linear_systems.cg import CG, cg_fm


@pytest.mark.parametrize(
    "a,b",
    [
        (np.identity(3), np.array([1, 1, 1])),
    ],
)
def test_cg_correct(a, b):
    CG(a=a, b=b)


@pytest.mark.parametrize(
    "a,b",
    [
        (np.identity(3), np.array([1, 1])),
        (np.identity(3), np.array([[1, 1], [1, 1]])),
    ],
)
def test_dims_validation_b(a, b):
    with pytest.raises(ValidationError):
        CG(a=a, b=b)


@pytest.mark.parametrize(
    "a,b,expected",
    [
        (
            np.array(
                [
                    [4, 3, 0],
                    [3, 4, -1],
                    [0, -1, 4],
                ]
            ),
            np.array([24, 30, -24]),
            np.array([3.0, 4.0, -5.0]),
        ),
    ],
)
def test_compute(a, b, expected):
    result_01 = CG(a=a, b=b).compute()
    result_02 = cg_fm(a, b)
    assert np.allclose(result_01, expected, 1e-05, 1e-08)
    assert np.allclose(result_02, expected, 1e-05, 1e-08)
