# -*- coding: utf-8 -*-
import pytest
import numpy as np
from pydantic import ValidationError

from fastmatrix.utils import (
    check_symmetric,
    positive_definite,
    SquareMatrix,
    SymmetricMatrix,
    PositiveDefiniteMatrix,
)


@pytest.mark.parametrize(
    "a",
    [
        (np.zeros((2, 2))),
        (np.ones((2, 2))),
    ],
)
def test_square_matrix_correct(a):
    SquareMatrix(a=a)


@pytest.mark.parametrize("a", [(np.zeros((2, 2, 2)))])
def test_dims_validation_a(a):
    with pytest.raises(ValidationError):
        SquareMatrix(a=a)


@pytest.mark.parametrize("a", [(np.zeros((2, 3)))])
def test_square_validation(a):
    with pytest.raises(ValidationError):
        SquareMatrix(a=a)


@pytest.mark.parametrize(
    "a",
    [
        (
            np.array(
                [
                    [1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 9],
                ]
            )
        )
    ],
)
def test_symmetric_validation(a):
    with pytest.raises(ValidationError):
        SymmetricMatrix(a=a)


@pytest.mark.parametrize(
    "a",
    [
        (
            np.array(
                [
                    [1, 2],
                    [2, 1],
                ]
            )
        )
    ],
)
def test_positive_definite_validation(a):
    with pytest.raises(ValidationError):
        PositiveDefiniteMatrix(a=a)


@pytest.mark.parametrize(
    "a,expected",
    [
        (
            np.array(
                [
                    [1, 2, 3],
                    [4, 5, 6],
                    [7, 8, 9],
                ]
            ),
            False,
        ),
        (
            np.array(
                [
                    [1, 2, 3],
                    [2, 5, 6],
                    [3, 6, 9],
                ]
            ),
            True,
        ),
    ],
)
def test_check_symmetric(a, expected):
    result = check_symmetric(a)
    assert result == expected


@pytest.mark.parametrize(
    "a,expected",
    [
        (
            np.array(
                [
                    [1, 0],
                    [0, 1],
                ]
            ),
            True,
        ),
        (
            np.array(
                [
                    [2, -1, 0],
                    [-1, 2, -1],
                    [0, -1, 2],
                ]
            ),
            True,
        ),
        (
            np.array(
                [
                    [1, 2],
                    [2, 1],
                ]
            ),
            False,
        ),
    ],
)
def test_positive_definite(a, expected):
    result = positive_definite(a)
    assert result == expected
