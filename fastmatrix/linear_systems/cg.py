# -*- coding: utf-8 -*-
import numpy as np
from pydantic import validator
from scipy.sparse.linalg import cg

from fastmatrix.utils import PositiveDefiniteMatrix
from deprecated import deprecated


class CG(PositiveDefiniteMatrix):
    """
    Class that define validations for `cg` function
    """

    b: np.ndarray

    class Config:
        arbitrary_types_allowed = True

    @validator("b")
    def dims_validation_b(cls, b, values):
        shape_b = b.shape
        shape_a = values["a"].shape
        assert len(shape_b) == 1, "'b' not is a vector"
        assert (
            shape_b[0] == shape_a[0]
        ), "'A' and 'b' have different dimensions"
        return b

    def compute(self):
        return cg(self.a, self.b, atol=1e-05)[0]


@deprecated(version="0.0.1", reason="Use `scipy.sparse.linalg.cg` instead")
def cg_fm(a: np.ndarray, b: np.ndarray) -> np.ndarray:
    """

    Description
    -----------
    Conjugate gradients (CG) method is an iterative algorithm
    for solving linear systems with positive
    definite coefficient matrices.
    More extensions and technical details
    can be found in :cite:`golub_1996`, :cite:`hestenes_1980`.



    Arguments
    ---------

    :param a: symmetric positive definite matrix containing
            the coefficients of the linear system.
    :param b: vector of right-hand sides of the linear system.
    :return: vector with the approximate solution, the iterations
            performed are returned as the attribute 'iterations'

    Examples
    --------

    .. code:: python

        from fastmatrix import cg_fm
        import numpy as np

        a = np.array([
             [4, 3, 0],
             [3, 4, -1],
             [0, -1, 4],
        ])
        b = np.array([24, 30, -24])

        result = cg_fm(a, b)
        result

    ::

        array([3, 4, -5])

    """

    return CG(a=a, b=b).compute()
