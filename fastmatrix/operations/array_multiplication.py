# -*- coding: utf-8 -*-
from pydantic import BaseModel, validator
import numpy as np


class ArrayMult(BaseModel):
    """
    Class that define validations for `array_mult` function
    """

    matrix_a: np.ndarray
    matrix_x: np.ndarray
    matrix_b: np.ndarray

    class Config:
        arbitrary_types_allowed = True

    @validator("matrix_a")
    def dims_validation_a(cls, matrix_a):
        shape_a = matrix_a.shape
        assert len(shape_a) == 2, "matrix 'A' not is 2-dimensional matrix"
        return matrix_a

    @validator("matrix_x")
    def dims_validation_x(cls, matrix_x, values):
        shape_a = values["matrix_a"].shape
        shape_x = matrix_x.shape
        assert len(shape_x) == 3, "matrix 'X' not is 3-dimensional matrix"
        assert (
            shape_a[1] == shape_x[1]
        ), "matrix 'A' not possible multiplicative with matrix 'X'"
        return matrix_x

    @validator("matrix_b")
    def dims_validation_b(cls, matrix_b, values):
        shape_x = values["matrix_x"].shape
        shape_b = matrix_b.shape
        assert len(shape_b) == 2, "matrix 'B' not is 2-dimensional matrix"
        assert (
            shape_x[2] == shape_b[0]
        ), "matrix 'X' not possible multiplicative with matrix 'B'"

        return matrix_b

    def compute(self) -> np.ndarray:

        a = self.matrix_a
        x = self.matrix_x
        b = self.matrix_b

        shape_a = a.shape
        shape_b = b.shape
        shape_x = x.shape

        y = np.zeros((shape_x[0], shape_a[0], shape_b[1]))
        for i in range(shape_x[0]):
            y[i, :, :] = a @ x[i, :, :] @ b

        return y


def array_mult(a: np.ndarray, x: np.ndarray, b: np.ndarray) -> np.ndarray:
    """
    Description
    -----------
    Multiplication of 3-dimensional arrays was first
    introduced by :cite:`bates_1980`.
    More extensions and technical details can be found in :cite:`wei_1998`.

    Details
    -------
    Let :math:`X = (x_{tij})` be a 3-dimensional :math:`n \\times p\\times q`
    where indices :math:`t, i` and :math:`j` indicate face, row and column,
    respectively. The product
    :math:`Y = AXB` is an :math:`n \\times r \\times s`
    array, with :math:`A` and :math:`B` are :math:`r \\times p`
    and :math:`q \\times s` matrices respectively.
    The elements of :math:`Y` are defined as:

    .. math::
        y_{tkl}=\\sum\\limits_{i=1}^p\\sum\\limits_{j=1}^q a_{ki}x_{tij}b_{jl}


    Arguments
    ---------

    :param a: 2-dimensional matrix
    :param x: 3-dimensional matrix
    :param b: 2-dimensional matrix
    :return: 3-dimensional array of dimension :math:`n \\times r \\times s`

    Examples
    --------

    .. code:: python

        from fastmatrix import array_mult
        import numpy as np

        a = np.ones((3,2))
        x = np.array([
            [[1,2,3], [2,4,6]],
            [[2,4,6],[4,8,12]],
            [[3,6,9],[6,12,18]]
        ])
        b = np.ones((3,2))

        result = array_mult(a,x,b)
        result

    ::

        array([[[18., 18.],
                [18., 18.],
                [18., 18.]],

               [[36., 36.],
                [36., 36.],
                [36., 36.]],

               [[54., 54.],
                [54., 54.],
                [54., 54.]]])

    """

    return ArrayMult(matrix_a=a, matrix_x=x, matrix_b=b).compute()
