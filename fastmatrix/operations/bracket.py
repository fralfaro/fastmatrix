# -*- coding: utf-8 -*-
from pydantic import BaseModel, validator
import numpy as np


class BracketProd(BaseModel):
    """
    Class that define validations for `bracket_prod` function
    """

    matrix_a: np.ndarray
    matrix_x: np.ndarray

    class Config:
        arbitrary_types_allowed = True

    @validator("matrix_a")
    def dims_validation_a(cls, matrix_a):
        shape_a = matrix_a.shape
        assert len(shape_a) == 2, "matrix 'A' not is 2-dimensional matrix"
        return matrix_a

    @validator("matrix_x")
    def dims_validation_x(cls, matrix_x, values):
        shape_a = values["matrix_a"].shape
        shape_x = matrix_x.shape
        assert len(shape_x) == 3, "matrix 'X' not is 3-dimensional matrix"
        assert (
            shape_a[1] == shape_x[0]
        ), "matrix 'A' not possible multiplicative with matrix 'X'"
        return matrix_x

    def compute(self) -> np.ndarray:

        a = self.matrix_a
        x = self.matrix_x

        shape_a = a.shape
        shape_x = x.shape

        y = np.zeros((shape_a[0], shape_x[1], shape_x[2]))

        for t in range(shape_a[0]):
            for i in range(shape_x[1]):
                y[t, i, :] = a[t, :] @ x[:, i, :]

        return y


def bracket_prod(a: np.ndarray, x: np.ndarray) -> np.ndarray:
    """
    Description
    -----------

    Bracket product of a matrix and a 3-dimensional array.
    More extensions and technical details can be found in :cite:`wei_1998`.

    Details
    -------

    Let :math:`X= (x_{tij})` be a 3-dimensional :math:`n \\times p \\times q`
    array and :math:`A` an :math:`m \\times n` matrix, then :math:`Y = [A][X]`
    is called the bracket product of :math:`A` and :math:`X` , that is an
    :math:`m \\times p \\times q` with elements

    .. math:: y_{tij} = \sum \limits_{k=1}^n a_{tk}x_{kij}

    Arguments
    ---------
    :param a: 2-dimensional matrix
    :param x: 3-dimensional matrix
    :return: 3-dimensional array of dimension  :math:`m \\times p \\times q`

    Examples
    --------

    .. code:: python

        from fastmatrix import bracket_prod
        import numpy as np

        a = np.ones((3,2))
        x = np.array([
            [[1, 2], [3, 4]],
            [[5,6], [7, 8]]
        ])

        result = bracket_prod(a,x)
        result

    ::

        array([[[ 6.,  8.],
                [10., 12.]],

               [[ 6.,  8.],
                [10., 12.]],

               [[ 6.,  8.],
                [10., 12.]]])

    """

    return BracketProd(matrix_a=a, matrix_x=x).compute()
