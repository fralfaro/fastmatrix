Python topics documented
========================

.. toctree::
    functions/array_mult
    functions/as_symmetric
    functions/bracket
    functions/cg
