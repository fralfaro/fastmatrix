Force a matrix to be symmetric
==============================

.. py:function:: as_symmetric(a:np.ndarray,lower:bool=True)-> np.ndarray
.. automodule:: fastmatrix.operations.symmetric.as_symmetric