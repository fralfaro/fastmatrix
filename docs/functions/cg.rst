Solve linear systems using the conjugate gradients method
==========================================================



.. py:function:: cg_fm(a: np.ndarray, b: np.ndarray) -> np.ndarray

.. warning::
    Deprecated since version 0.0.1: Use `scipy.sparse.linalg.cg` instead.


.. automodule:: fastmatrix.linear_systems.cg.cg_fm