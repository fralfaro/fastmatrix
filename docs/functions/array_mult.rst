Array multiplication
====================

.. py:function:: array_mult(a:np.ndarray,x:np.ndarray,b:np.ndarray) -> np.ndarray
.. automodule:: fastmatrix.operations.array_multiplication.array_mult
